package main.fabricas;

import main.usuario.AlunoGraduacao;
import main.usuario.AlunoPosGraduacao;
import main.usuario.Professor;
import main.usuario.Usuario;

public class FabricaUsuario {
    public static Usuario criaUsuario(String nome, String id, TiposUsuario tipo) {
        switch (tipo) {
            case PROFESSOR -> {
                return new Professor(nome, id);
            }
            case ALUNO_POS_GRADUACAO -> {
                return new AlunoPosGraduacao(nome, id);
            }
            default -> {
                return new AlunoGraduacao(nome, id);
            }
        }
    }
}

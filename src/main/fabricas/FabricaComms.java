package main.fabricas;

import main.comunicacao.*;

public class FabricaComms {
    public static IComunicacao criaComms() {
        IComunicacao comms = new Terminal();
        comms.registraComando("sai", new ComandoSair());
        comms.registraComando("emp", new ComandoEmprestimo());
        comms.registraComando("dev", new ComandoDevolverEmprestimo());
        comms.registraComando("res", new ComandoReservar());
        comms.registraComando("obs", new ComandoObservavel());
        comms.registraComando("liv", new ComandoConsultaLivro());
        comms.registraComando("usu", new ComandoConsultaUsuario());
        comms.registraComando("ntf", new ComandoConsultaAdministrador());
        return comms;
    }
}

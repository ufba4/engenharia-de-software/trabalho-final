package main.fabricas;

import main.objetos.Livro;
import main.objetos.Objeto;

import java.util.ArrayList;

public class FabricaObjeto {
    public static Objeto criaObjeto(String id, String nome, ArrayList<String> nomeAutores, int anoPublicacao, String editora, String versao, int numeroTotal) {
        return new Livro(id, nome, nomeAutores, anoPublicacao, editora, versao, numeroTotal);
    }
}

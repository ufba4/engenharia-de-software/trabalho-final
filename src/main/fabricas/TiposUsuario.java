package main.fabricas;

public enum TiposUsuario {
    ALUNO_GRADUACAO,
    ALUNO_POS_GRADUACAO,
    PROFESSOR
}

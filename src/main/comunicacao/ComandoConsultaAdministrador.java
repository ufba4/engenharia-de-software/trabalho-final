package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.IAdministrador;
import main.verificacoes.VerificaInput;

public class ComandoConsultaAdministrador implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            IAdministrador a = VerificaInput.validaAdministrador(commandTotal, sistema);
            return "Notificacoes: " + a.getNumeroNotificacoes();
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: liv 100"));
        }  catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

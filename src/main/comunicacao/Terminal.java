package main.comunicacao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Terminal implements IComunicacao {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final HashMap<String, IComando> comandos = new HashMap<>();

    @Override
    public void pegaComando() {
        try {
            String command = this.recebeInformacao("Comando:");

            // Split string no espaco e pega primeiro elemento (Chave do comando)
            String commandKey = command.split("\\s+")[0];

            if (!this.comandos.containsKey(commandKey)) {
                this.mandaInformacao("Comando nao encontrado! Os comandos disponiveis sao:");
                this.listarComandos();
                return;
            }

            String resp = this.comandos.get(commandKey).executar(command);
            this.mandaInformacao(resp);
        } catch (IOException e) {
            System.out.println("Erro ao pegar o comando!");
            System.exit(1);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void registraComando(String chave, IComando comando) {
        this.comandos.put(chave, comando);
    }

    @Override
    public void mandaInformacao(String s) {
        System.out.println(s);
    }

    @Override
    public String recebeInformacao(String s) throws IOException {
        System.out.println(s);
        return this.reader.readLine();
    }

    @Override
    public void listarComandos() {
        for (String command : this.comandos.keySet()) {
            System.out.println(command);
        }
    }

    @Override
    public void init() {
        while (true) {
            this.pegaComando();
        }
    }
}

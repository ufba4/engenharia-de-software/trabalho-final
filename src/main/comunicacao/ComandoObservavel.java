package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.IAdministrador;
import main.verificacoes.VerificaInput;

public class ComandoObservavel implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Tuple2<IAdministrador, Objeto> tuple = VerificaInput.validaAdministradorEObjeto(commandTotal, sistema);
            IAdministrador a = tuple.pegaPrimeiroElemento();
            Objeto o = tuple.pegaSegundoElemento();
            if (!o.objetoObservavel()) {
                throw new BusinessException("O Objeto nao pode ser observado!");
            }

            o.adicionaObservador(a);
            return "Observador registrado com sucesso!";
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: obs 123 100"));
        }  catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

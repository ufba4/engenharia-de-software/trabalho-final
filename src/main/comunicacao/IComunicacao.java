package main.comunicacao;

import java.io.IOException;

public interface IComunicacao {
    void pegaComando();

    void mandaInformacao(String s);

    String recebeInformacao(String s) throws IOException;

    void registraComando(String nome, IComando comando);

    void listarComandos();

    void init();
}

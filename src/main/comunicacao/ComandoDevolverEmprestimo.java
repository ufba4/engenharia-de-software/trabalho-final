package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.Usuario;
import main.verificacoes.VerificaInput;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class ComandoDevolverEmprestimo implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Tuple2<Usuario, Objeto> tuple = VerificaInput.validaUsuarioEObjeto(commandTotal, sistema);
            Usuario u = tuple.pegaPrimeiroElemento();
            Objeto o = tuple.pegaSegundoElemento();

            sistema.removeEmprestimo(u, o);
            return getUsuarioEObjetoReturnMessage("Livro devolvido", u, o);
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: dev 123 100"));
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

package main.comunicacao;

import main.objetos.Objeto;
import main.usuario.Usuario;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MessageUtils {
    public static String getUsuarioEObjetoReturnMessage(String message, Usuario u, Objeto o) {
        return message.concat("\nUsuario: ").concat(u.getNome()).concat("\nLivro: ").concat(o.getNome());
    }

    public static String getFormattedDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return date.format(formatter);
    }
}

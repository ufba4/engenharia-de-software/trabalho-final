package main.comunicacao;

public class ComandoSair implements IComando {
    @Override
    public String executar(String commandTotal) {
        System.out.println("Saindo do sistema");
        System.exit(0);
        return null;
    }
}

package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.Usuario;
import main.verificacoes.VerificaInput;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class ComandoEmprestimo implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Tuple2<Usuario, Objeto> tuple = VerificaInput.validaUsuarioEObjeto(commandTotal, sistema);
            Usuario u = tuple.pegaPrimeiroElemento();
            Objeto o = tuple.pegaSegundoElemento();

            if (!o.objetoEmprestavel()) {
                throw new BusinessException(getUsuarioEObjetoReturnMessage("O item nao pode ser emprestado", u, o));
            }

            if (!o.podeRealizarEmprestimo()) {
                throw new BusinessException(getUsuarioEObjetoReturnMessage("Nao existem exemplares deste livro disponiveis para emprestimo", u, o));
            }

            Tuple2<Boolean, String> respUsuario = u.podePegarEmprestimo(sistema, o);
            if (!respUsuario.pegaPrimeiroElemento()) {
                throw new BusinessException(respUsuario.pegaSegundoElemento());
            }

            sistema.adicionaEmprestimo(u, o);
            return getUsuarioEObjetoReturnMessage("Emprestimo realizado com sucesso!", u, o);
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: emp 123 100"));
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

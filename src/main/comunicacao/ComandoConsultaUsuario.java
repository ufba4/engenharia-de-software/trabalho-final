package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.Usuario;
import main.verificacoes.VerificaInput;

public class ComandoConsultaUsuario implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Usuario u = VerificaInput.validaUsuario(commandTotal, sistema);
            return u.getObjectInfo();
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: usu 123"));
        }  catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

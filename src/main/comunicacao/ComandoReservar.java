package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.Usuario;
import main.verificacoes.VerificaInput;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class ComandoReservar implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Tuple2<Usuario, Objeto> tuple = VerificaInput.validaUsuarioEObjeto(commandTotal, sistema);
            Usuario u = tuple.pegaPrimeiroElemento();
            Objeto o = tuple.pegaSegundoElemento();

            if (!o.objetoReservavel()) {
                throw new BusinessException(getUsuarioEObjetoReturnMessage("Objeto nao pode ser reservado", u, o));
            }

            if (u.getMaxReservas() < u.getReservas().size() + 1) {
                throw new BusinessException(getUsuarioEObjetoReturnMessage("Limite de reservas atingido",u,o));
            }

            sistema.adicionaReserva(u, o);
            return getUsuarioEObjetoReturnMessage("Reserva realizada com sucesso", u, o);
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: res 123 100"));
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

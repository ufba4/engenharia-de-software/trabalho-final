package main.comunicacao;

import main.exceptions.BusinessException;

public interface IComando {
    String executar(String commandTotal) throws BusinessException;
}

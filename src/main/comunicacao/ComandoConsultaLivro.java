package main.comunicacao;

import main.exceptions.BusinessException;
import main.exceptions.InvalidCommandException;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.verificacoes.VerificaInput;

public class ComandoConsultaLivro implements IComando {
    @Override
    public String executar(String commandTotal) throws BusinessException {
        try {
            IDataSalvo sistema = Sistema.getInstance();
            Objeto o = VerificaInput.validaObjeto(commandTotal, sistema);
            return o.getObjectInfo();
        } catch (InvalidCommandException e) {
            throw new BusinessException(e.getMessage().concat("\nExemplo valido: liv 100"));
        }  catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }
}

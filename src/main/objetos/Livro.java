package main.objetos;

import main.objetos.comportamentos.ObjetoEmprestavel;
import main.objetos.comportamentos.ObjetoObservavel;
import main.objetos.comportamentos.ObjetoReservavel;
import main.sistema.Emprestimo;
import main.sistema.Reserva;

import java.util.ArrayList;

import static main.comunicacao.MessageUtils.getFormattedDate;

public class Livro extends Objeto {
    private final ArrayList<String> nomeAutores;
    private final String editora;
    private final String versao;

    public Livro(String id, String nome, ArrayList<String> nomeAutores, int anoPublicacao, String editora, String versao, int numeroTotal) {
        super(id, nome, anoPublicacao, new ObjetoEmprestavel(numeroTotal), new ObjetoObservavel(2), new ObjetoReservavel());
        this.nomeAutores = nomeAutores;
        this.editora = editora;
        this.versao = versao;
    }

    public ArrayList<String> getNomeAutores() {
        return nomeAutores;
    }

    public String getEditora() {
        return editora;
    }

    public String getVersao() {
        return versao;
    }

    @Override
    public void realizaEmprestimo(Emprestimo e) {
        super.realizaEmprestimo(e);

        Reserva r = this.getReservaPorIdUsuario(e.getUsuario().getId());
        if (r != null) {
            this.removeReserva(r);
        }
    }

    @Override
    public void reservar(Reserva r) {
        super.reservar(r);
        this.getObservavel().notify(this.getReservavel().getNumeroReservas(), r);
    }

    @Override
    public String getObjectInfo() {
        StringBuilder resp = new StringBuilder();
        resp.append("Titulo: ").append(this.getNome()).append("\n");
        resp.append("Quantidade de Reservas: ").append(this.getReservavel().getNumeroReservas()).append("\n");
        for (Reserva r : this.getReservavel().getReservas()) {
            resp.append("Reservado por: ").append(r.getUsuario().getNome()).append("\n");
        }

        resp.append("Exemplares\n");
        for (Exemplar e : this.getEmprestavel().getEmprestimoTable().keySet()) {
            resp.append(e);
            if (this.getEmprestavel().getEmprestimoExemplar(e) == null) {
                resp.append(" - Disponivel").append("\n");
            } else {
                Emprestimo emprestimo = this.getEmprestavel().getEmprestimoExemplar(e);
                resp.append(" - Emprestado - ").append(emprestimo.getUsuario().getNome())
                        .append(" | Emprestado em ").append(getFormattedDate(emprestimo.getDataEmprestimo()))
                        .append(" | Previsao de devolucao em ").append(getFormattedDate(emprestimo.getDataEntregaPrevista())).append("\n");
            }
        }

        return resp.toString();
    }
}
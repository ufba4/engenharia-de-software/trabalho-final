package main.objetos;

// A record can hold data with more performance than a class with the same function.
public record Exemplar(String id) {

    @Override
    public String toString() {
        return "Exemplar(codigo=" + id + ")";
    }
}

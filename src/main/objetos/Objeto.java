package main.objetos;

import main.comunicacao.IImprimivel;
import main.objetos.comportamentos.*;
import main.sistema.Emprestimo;
import main.sistema.Reserva;
import main.usuario.IAdministrador;

import java.util.ArrayList;

public abstract class Objeto implements IImprimivel {
    private final String id;
    private final String nome;
    private final int anoPublicacao;
    private final IEmprestavel emprestavel;
    private final IObservavel observavel;
    private final IReservavel reservavel;

    public Objeto(String id, String nome, int anoPublicacao, IEmprestavel emprestavel, IObservavel observavel, IReservavel reservavel) {
        this.id = id;
        this.nome = nome;
        this.anoPublicacao = anoPublicacao;
        this.emprestavel = emprestavel;
        this.observavel = observavel;
        this.reservavel = reservavel;
    }

    public String getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public int getAnoPublicacao() {
        return this.anoPublicacao;
    }

    abstract public String getObjectInfo();

    public IEmprestavel getEmprestavel() {
        return emprestavel;
    }

    public IObservavel getObservavel() {
        return observavel;
    }

    public IReservavel getReservavel() {
        return reservavel;
    }

    public Exemplar getExemplar(String id) {
        return this.getEmprestavel().getExemplar(id);
    }

    public void realizaEmprestimo(Emprestimo e) {
        this.getEmprestavel().realizaEmprestimo(e);
    }

    public boolean podeRealizarEmprestimo() {
        return this.getEmprestavel().podeRealizarEmprestimo();
    }

    public void removerEmprestimo(Emprestimo e) throws Error {
        this.getEmprestavel().removerEmprestimo(e);
    }

    public Emprestimo getEmprestimoIdUsuario(String id) {
        return this.getEmprestavel().getEmprestimoIdUsuario(id);
    }

    public ArrayList<Emprestimo> getEmprestimos() {
        return this.getEmprestavel().getEmprestimos();
    }

    public int getNumeroDisponiveis() {
        return this.getEmprestavel().getNumeroDisponiveis();
    }

    public void reservar(Reserva r) {
        this.getReservavel().reservar(r);
    }

    public void removeReserva(Reserva r) {
        this.getReservavel().removeReserva(r);
    }

    public ArrayList<Reserva> getReservas() {
        return this.getReservavel().getReservas();
    }

    public Reserva getReservaPorIdUsuario(String id) {
        return this.getReservavel().getReservaPorIdUsuario(id);
    }

    public int getNumeroReservas() {
        return this.getReservavel().getNumeroReservas();
    }

    public void adicionaObservador(IAdministrador administrador) {
        this.getObservavel().adicionaObservador(administrador);
    }

    public void removeObservador(IAdministrador administrador) {
        this.getObservavel().removeObservador(administrador);
    }

    public boolean objetoObservavel() {
        return this.observavel.objetoObservavel();
    }

    public boolean objetoEmprestavel() {
        return this.emprestavel.objetoEmprestavel();
    }

    public boolean objetoReservavel() {
        return this.reservavel.objetoReservavel();
    }
}

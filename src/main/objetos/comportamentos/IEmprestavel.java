package main.objetos.comportamentos;

import main.objetos.Exemplar;
import main.sistema.Emprestimo;

import java.util.ArrayList;
import java.util.HashMap;

public interface IEmprestavel {
    void realizaEmprestimo(Emprestimo e);

    boolean podeRealizarEmprestimo();

    void removerEmprestimo(Emprestimo e) throws Error;

    Emprestimo getEmprestimoIdUsuario(String id);

    ArrayList<Emprestimo> getEmprestimos();

    Exemplar getExemplar(String id);

    int getNumeroDisponiveis();

    HashMap<Exemplar, Emprestimo> getEmprestimoTable();

    Emprestimo getEmprestimoExemplar(Exemplar e);

    boolean objetoEmprestavel();
}

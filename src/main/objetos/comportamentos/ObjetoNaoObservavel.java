package main.objetos.comportamentos;

import main.sistema.Reserva;
import main.usuario.IAdministrador;

public class ObjetoNaoObservavel implements IObservavel{
    @Override
    public void adicionaObservador(IAdministrador a) {

    }

    @Override
    public void removeObservador(IAdministrador a) {

    }

    @Override
    public void notify(int amountReservas, Reserva r) {

    }

    @Override
    public boolean objetoObservavel() {
        return false;
    }
}

package main.objetos.comportamentos;

import main.eventos.MockNotificacao;
import main.sistema.Reserva;
import main.usuario.IAdministrador;

import java.util.ArrayList;

public class ObjetoObservavel implements IObservavel{
    private final ArrayList<IAdministrador> observadores = new ArrayList<>();
    private final int numeroReservasNotificar;

    public ObjetoObservavel(int numeroReservasNotificar) {
        this.numeroReservasNotificar = numeroReservasNotificar;
    }

    @Override
    public void adicionaObservador(IAdministrador a) {
        this.observadores.add(a);
    }

    @Override
    public void removeObservador(IAdministrador administrador) {
        for (IAdministrador a : this.observadores) {
            if (a.getId().equals(administrador.getId())) {
                this.observadores.remove(a);
                break;
            }
        }
    }

    @Override
    public void notify(int amountReservas, Reserva r) {
        if (amountReservas >= this.getNumeroReservasNotificar()) {
            System.out.println("Notificacao: O livro " + r.getObjeto().getNome() + " possui " + amountReservas + " reservas ativas");
            for (IAdministrador a : this.getObservadores()) {
                MockNotificacao.sendNotificacao(a);
            }
        }
    }

    public ArrayList<IAdministrador> getObservadores() {
        return this.observadores;
    }

    public int getNumeroReservasNotificar() {
        return this.numeroReservasNotificar;
    }

    @Override
    public boolean objetoObservavel() {
        return true;
    }
}

package main.objetos.comportamentos;

import main.objetos.Exemplar;
import main.sistema.Emprestimo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class ObjetoEmprestavel implements IEmprestavel {
    private final HashMap<Exemplar, Emprestimo> emprestimos = new HashMap<>();

    public ObjetoEmprestavel(int numeroTotal) {
        Random rnd = new Random();
        for (int i = 0; i < numeroTotal; i++) {
            int number = rnd.nextInt(999999);
            String exemplarId = String.format("%06d", number);;
            Exemplar e = new Exemplar(exemplarId);
            this.emprestimos.put(e, null);
        }
    }

    @Override
    public void realizaEmprestimo(Emprestimo e) {
        for (Exemplar i : this.emprestimos.keySet()) {
            if (this.emprestimos.get(i) == null) {
                this.emprestimos.put(i, e);
                break;
            }
        }

    }

    @Override
    public boolean podeRealizarEmprestimo() {
        for (Exemplar i : this.emprestimos.keySet()) {
            if (this.emprestimos.get(i) == null) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void removerEmprestimo(Emprestimo e) throws Error {
        if (this.getEmprestimoIdUsuario(e.getUsuario().getId()) != null) {
            for (Exemplar i : this.emprestimos.keySet()) {
                Emprestimo emprestimo = this.emprestimos.get(i);
                if (emprestimo != null && this.emprestimos.get(i).getUsuario().getId().equals(e.getUsuario().getId())) {
                    this.emprestimos.put(i, null);
                }
            }
            return;
        }

        throw new Error("Tentando devolver livro que nao pertence a biblioteca");
    }

    @Override
    public Emprestimo getEmprestimoIdUsuario(String id) {
        for (Exemplar i : this.emprestimos.keySet()) {
            Emprestimo e = this.emprestimos.get(i);
            if (e != null && e.getUsuario().getId().equals(id)) {
                return e;
            }
        }

        return null;
    }

    @Override
    public ArrayList<Emprestimo> getEmprestimos() {
        ArrayList<Emprestimo> arrayList = new ArrayList<>();
        for (Exemplar i : this.emprestimos.keySet()) {
            arrayList.add(this.emprestimos.get(i));
        }

        return arrayList;
    }

    @Override
    public Exemplar getExemplar(String id) {
        for (Exemplar i : this.emprestimos.keySet()) {
            if (i.id().equals(id)) return i;
        }

        return null;
    }

    @Override
    public int getNumeroDisponiveis() {
        int res = 0;

        for (Exemplar i : this.emprestimos.keySet()) {
            if (this.emprestimos.get(i) == null) {
                res++;
            }
        }

        return res;
    }

    @Override
    public HashMap<Exemplar, Emprestimo> getEmprestimoTable() {
        return this.emprestimos;
    }

    @Override
    public Emprestimo getEmprestimoExemplar(Exemplar e) {
        return this.emprestimos.get(e);
    }

    @Override
    public boolean objetoEmprestavel() {
        return true;
    }
}

package main.objetos.comportamentos;

import main.sistema.Reserva;

import java.util.ArrayList;

public interface IReservavel {
    void reservar(Reserva r);

    void removeReserva(Reserva r);

    ArrayList<Reserva> getReservas();

    Reserva getReservaPorIdUsuario(String id);

    int getNumeroReservas();

    boolean objetoReservavel();
}

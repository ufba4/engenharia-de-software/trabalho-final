package main.objetos.comportamentos;

import main.sistema.Reserva;
import main.usuario.IAdministrador;

public interface IObservavel {
    void adicionaObservador(IAdministrador a);

    void removeObservador(IAdministrador a);

    void notify(int amountReservas, Reserva r);

    boolean objetoObservavel();
}

package main.objetos.comportamentos;

import main.sistema.Reserva;

import java.util.ArrayList;

public class ObjetoNaoReservavel implements IReservavel{
    @Override
    public void reservar(Reserva r) {

    }

    @Override
    public void removeReserva(Reserva r) {

    }

    @Override
    public ArrayList<Reserva> getReservas() {
        return null;
    }

    @Override
    public Reserva getReservaPorIdUsuario(String id) {
        return null;
    }

    @Override
    public int getNumeroReservas() {
        return 0;
    }

    @Override
    public boolean objetoReservavel() {
        return false;
    }
}

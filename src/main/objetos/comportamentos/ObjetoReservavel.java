package main.objetos.comportamentos;

import main.sistema.Reserva;

import java.util.ArrayList;

public class ObjetoReservavel implements IReservavel{
    private final ArrayList<Reserva> reservas = new ArrayList<>();

    @Override
    public void reservar(Reserva r) {
        this.reservas.add(r);
    }

    @Override
    public void removeReserva(Reserva r) {
        this.reservas.remove(r);
    }

    @Override
    public ArrayList<Reserva> getReservas() {
        return this.reservas;
    }

    @Override
    public Reserva getReservaPorIdUsuario(String id) {
        for (Reserva r : this.reservas) {
            if (r.getUsuario().getId().equals(id)) {
                return r;
            }
        }

        return null;
    }

    @Override
    public int getNumeroReservas() {
        return this.reservas.size();
    }

    @Override
    public boolean objetoReservavel() {
        return true;
    }
}

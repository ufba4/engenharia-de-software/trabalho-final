package main.objetos.comportamentos;

import main.objetos.Exemplar;
import main.sistema.Emprestimo;

import java.util.ArrayList;
import java.util.HashMap;

public class ObjetoNaoEmprestavel implements IEmprestavel {
    @Override
    public void realizaEmprestimo(Emprestimo e) {

    }

    @Override
    public boolean podeRealizarEmprestimo() {
        return false;
    }

    @Override
    public void removerEmprestimo(Emprestimo e) throws Error {

    }

    @Override
    public Emprestimo getEmprestimoIdUsuario(String id) {
        return null;
    }

    @Override
    public ArrayList<Emprestimo> getEmprestimos() {
        return null;
    }

    @Override
    public Exemplar getExemplar(String id) {
        return null;
    }

    @Override
    public int getNumeroDisponiveis() {
        return 0;
    }

    @Override
    public HashMap<Exemplar, Emprestimo> getEmprestimoTable() {
        return null;
    }

    @Override
    public Emprestimo getEmprestimoExemplar(Exemplar e) {
        return null;
    }

    @Override
    public boolean objetoEmprestavel() {
        return false;
    }
}

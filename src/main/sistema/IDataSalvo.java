package main.sistema;

import main.objetos.Objeto;
import main.usuario.IAdministrador;
import main.usuario.Usuario;

import java.util.ArrayList;
import java.util.HashMap;

public interface IDataSalvo {
    void adicionarUsuario(Usuario u);

    Usuario pegarUsuarioPorId(String id);

    void adicionarObjeto(Objeto o);

    Objeto pegarObjetoPorId(String id);

    void adicionarAdministrador(IAdministrador a);

    IAdministrador pegarAdministradorPorId(String id);

    Emprestimo adicionaEmprestimo(Usuario u, Objeto o);

    void removeEmprestimo(Usuario u, Objeto o) throws Exception;

    Reserva adicionaReserva(Usuario u, Objeto o) throws Exception;

    ArrayList<Emprestimo> pegaEmprestimosIdObjeto(String id);

    ArrayList<Reserva> pegaReservaIdObjeto(String id);

    HashMap<String, Objeto> getObjetos();
}

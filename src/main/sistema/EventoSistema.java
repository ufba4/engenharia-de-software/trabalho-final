package main.sistema;

import main.objetos.Objeto;
import main.usuario.Usuario;

public abstract class EventoSistema {
    private final Usuario usuario;
    private final Objeto objeto;

    public EventoSistema(Usuario usuario, Objeto objeto) {
        this.usuario = usuario;
        this.objeto = objeto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Objeto getObjeto() {
        return objeto;
    }
}

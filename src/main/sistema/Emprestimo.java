package main.sistema;

import main.objetos.Objeto;
import main.usuario.Usuario;

import java.time.LocalDate;

import static java.time.LocalDate.now;

public class Emprestimo extends EventoSistema {
    private final LocalDate dataEmprestimo;
    private final LocalDate dataEntregaPrevista;
    private LocalDate dataEntrega;

    public Emprestimo(Usuario usuario, Objeto objeto) {
        super(usuario, objeto);
        this.dataEmprestimo = now();
        this.dataEntregaPrevista = this.dataEmprestimo.plusDays(usuario.getDiasEmprestimo());
    }

    public void devolve() {
        this.dataEntrega = now();
    }

    public LocalDate getDataEmprestimo() {
        return dataEmprestimo;
    }

    public LocalDate getDataEntregaPrevista() {
        return dataEntregaPrevista;
    }

    public LocalDate getDataEntrega() {
        return dataEntrega;
    }
}

package main.sistema;

import main.objetos.Objeto;
import main.usuario.Usuario;

import java.time.LocalDate;

import static java.time.LocalDate.now;

public class Reserva extends EventoSistema {
    private final LocalDate dataSolicitacao;

    public Reserva(Usuario usuario, Objeto objeto) {
        super(usuario, objeto);
        this.dataSolicitacao = now();
    }

    public LocalDate getDataEmprestimo() {
        return this.dataSolicitacao;
    }
}

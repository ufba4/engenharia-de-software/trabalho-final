package main.sistema;

import main.exceptions.BusinessException;
import main.objetos.Objeto;
import main.usuario.IAdministrador;
import main.usuario.Usuario;

import java.util.ArrayList;
import java.util.HashMap;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class Sistema implements IDataSalvo {
    static Sistema sistema;
    private final HashMap<String, Objeto> objetos = new HashMap<>();
    private final HashMap<String, Usuario> usuarios = new HashMap<>();
    private final HashMap<String, IAdministrador> administradores = new HashMap<>();

    private Sistema() {
    }

    public static synchronized Sistema getInstance() {
        if (sistema == null) {
            sistema = new Sistema();
        }

        return sistema;
    }

    @Override
    public void adicionarUsuario(Usuario u) {
        this.usuarios.put(u.getId(), u);
    }

    @Override
    public Usuario pegarUsuarioPorId(String id) {
        return this.usuarios.getOrDefault(id, null);
    }

    @Override
    public void adicionarAdministrador(IAdministrador a) {
        this.administradores.put(a.getId(), a);
    }

    @Override
    public IAdministrador pegarAdministradorPorId(String id) {
        return this.administradores.getOrDefault(id, null);
    }

    @Override
    public void adicionarObjeto(Objeto o) {
        this.objetos.put(o.getId(), o);
    }

    @Override
    public Objeto pegarObjetoPorId(String id) {
        return this.objetos.getOrDefault(id, null);
    }

    @Override
    public Emprestimo adicionaEmprestimo(Usuario u, Objeto o) {
        Emprestimo e = new Emprestimo(u, o);
        u.adicionaEmprestimo(e);
        o.realizaEmprestimo(e);

        return e;
    }

    @Override
    public Reserva adicionaReserva(Usuario u, Objeto o) throws BusinessException {
        Reserva r = new Reserva(u, o);
        u.adicionaReserva(r);
        o.reservar(r);

        return r;
    }

    @Override
    public ArrayList<Emprestimo> pegaEmprestimosIdObjeto(String id) {
        Objeto o = this.pegarObjetoPorId(id);

        if (!o.objetoEmprestavel()) {
            throw new Error("Esse item nao pode ser emprestado");
        }

        return o.getEmprestimos();
    }

    @Override
    public ArrayList<Reserva> pegaReservaIdObjeto(String id) {
        Objeto o = this.pegarObjetoPorId(id);

        if (!o.objetoReservavel()) {
            throw new Error("Esse item nao pode ser reservado");
        }

        return o.getReservas();
    }

    @Override
    public void removeEmprestimo(Usuario u, Objeto o) throws BusinessException {
        if (!o.objetoEmprestavel()) {
            throw new BusinessException(getUsuarioEObjetoReturnMessage("O item nao pode ser emprestado", u, o));
        }

        Emprestimo emprestimo = o.getEmprestimoIdUsuario(u.getId());
        if (emprestimo == null) {
            throw new BusinessException(getUsuarioEObjetoReturnMessage("Este item nao foi emprestado para esse usuario", u, o));
        }

        emprestimo.devolve();
        o.removerEmprestimo(emprestimo);
        u.removeEmprestimo(emprestimo);
    }

    @Override
    public HashMap<String, Objeto> getObjetos() {
        return objetos;
    }
}

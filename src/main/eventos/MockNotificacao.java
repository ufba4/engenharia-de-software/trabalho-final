package main.eventos;

import main.usuario.IAdministrador;

public class MockNotificacao implements INotifica {
    public static void sendNotificacao(IAdministrador a) {
        System.out.println("Administrador(codigo=".concat(a.getId()).concat(") notificado"));
        a.notifica();
    }
}

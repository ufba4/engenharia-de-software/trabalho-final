package main;

import main.comunicacao.IComunicacao;
import main.fabricas.TiposUsuario;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.sistema.Sistema;
import main.usuario.IAdministrador;
import main.usuario.Usuario;

import java.util.ArrayList;
import java.util.List;

import static main.fabricas.FabricaComms.criaComms;
import static main.fabricas.FabricaObjeto.criaObjeto;
import static main.fabricas.FabricaUsuario.criaUsuario;

public class Main {
    public static void main(String[] args) {
        // Dados mockados
        IDataSalvo sistema = Sistema.getInstance();
        // Registrando livros
        Objeto l1 = criaObjeto("100", "Engenharia de Software", new ArrayList<>(List.of("Ian Sommervile")), 2000, "AddisonWesley", "6", 2);
        Objeto l2 = criaObjeto("101", "UML – Guia do Usuário", new ArrayList<>(List.of("Grady Booch", "James Rumbaugh", "Ivar Jacobson")), 2000, "Campus", "7", 1);
        Objeto l3 = criaObjeto("200", "Code Complete", new ArrayList<>(List.of("Steve McConnell")), 2014, "Microsoft Press", "2", 1);
        Objeto l4 = criaObjeto("201", "Agile Software Development, Principles, Patterns, and Practices", new ArrayList<>(List.of("Robert Martin")), 2002, "Pratice Hall", "1", 1);
        Objeto l5 = criaObjeto("300", "Refactoring: Improving the Design of Existing Code", new ArrayList<>(List.of("Martin Fowler")), 1999, "Addison-Wesley Professional", "1", 2);
        Objeto l6 = criaObjeto("301", "Software Metrics: A Rigorous and Practical Approach", new ArrayList<>(List.of("Norman Fenton", "James Bieman")), 2014, "CRC Press", "3", 0);
        Objeto l7 = criaObjeto("400", "Design Patterns: Elements of Reusable Object-Oriented Software", new ArrayList<>(List.of("Erich Gamma,", "Richard Helm", "Ralph Johnson", "John Vlissides")), 1994, "Addison-Wesley Professional", "1", 2);
        Objeto l8 = criaObjeto("401", "UML Distilled: A Brief Guide to the Standard Object Modeling Language", new ArrayList<>(List.of("Martin Fowler")), 2003, "Addison-Wesley Professional", "3", 0);
        sistema.adicionarObjeto(l1);
        sistema.adicionarObjeto(l2);
        sistema.adicionarObjeto(l3);
        sistema.adicionarObjeto(l4);
        sistema.adicionarObjeto(l5);
        sistema.adicionarObjeto(l6);
        sistema.adicionarObjeto(l7);
        sistema.adicionarObjeto(l8);
        // Registrando usuarios
        Usuario u1 = criaUsuario("Joao da Silva", "123", TiposUsuario.ALUNO_GRADUACAO);
        Usuario u2 = criaUsuario("Luiz Fernando Rodrigues", "456", TiposUsuario.ALUNO_POS_GRADUACAO);
        Usuario u3 = criaUsuario("Pedro Paulo", "789", TiposUsuario.ALUNO_GRADUACAO);
        Usuario u4 = criaUsuario("Carlos Lucena", "100", TiposUsuario.PROFESSOR);
        sistema.adicionarAdministrador((IAdministrador) u4);
        sistema.adicionarUsuario(u1);
        sistema.adicionarUsuario(u2);
        sistema.adicionarUsuario(u3);
        sistema.adicionarUsuario(u4);

        // Registra terminal
        IComunicacao comms = criaComms();
        comms.init();
    }
}
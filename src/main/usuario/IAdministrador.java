package main.usuario;

public interface IAdministrador {
    String getId();
    void notifica();
    int getNumeroNotificacoes();
}

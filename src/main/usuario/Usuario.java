package main.usuario;

import main.comunicacao.IImprimivel;
import main.exceptions.BusinessException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.Emprestimo;
import main.sistema.IDataSalvo;
import main.sistema.Reserva;

import java.time.LocalDate;
import java.util.ArrayList;

import static main.comunicacao.MessageUtils.getFormattedDate;
import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public abstract class Usuario implements IImprimivel {
    private final String id;
    private final IValidaEmprestimo validaEmprestimo;
    private String nome;
    private int diasEmprestimo;
    private int maxEmprestimos;
    private int maxReservas;
    private final ArrayList<Emprestimo> emprestimos = new ArrayList<>();
    private final ArrayList<Emprestimo> emprestimosDevolvidos = new ArrayList<>();
    private final ArrayList<Reserva> reservas = new ArrayList<>();

    public Usuario(String nome, String id, IValidaEmprestimo validaEmprestimo) {
        this.nome = nome;
        this.id = id;
        this.validaEmprestimo = validaEmprestimo;
        this.maxReservas = 3;
        this.diasEmprestimo = 0;
        this.maxEmprestimos = 0;
    }

    public Tuple2<Boolean, String> podePegarEmprestimo(IDataSalvo s, Objeto o) {
        return this.validaEmprestimo.validaEmprestimo(s, o, this);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public boolean isDevedor() {
        LocalDate refTime = LocalDate.now();
        for (Emprestimo e : this.emprestimos) {
            if (e.getDataEntregaPrevista().isBefore(refTime)) {
                return true;
            }
        }

        return false;
    }

    public int getDiasEmprestimo() {
        return diasEmprestimo;
    }

    public void setDiasEmprestimo(int diasEmprestimo) {
        this.diasEmprestimo = diasEmprestimo;
    }

    public int getMaxEmprestimos() {
        return maxEmprestimos;
    }

    public void setMaxEmprestimos(int maxEmprestimos) {
        this.maxEmprestimos = maxEmprestimos;
    }

    public void adicionaEmprestimo(Emprestimo e) {
        this.emprestimos.add(e);

        for (Reserva r : this.reservas) {
            if (r.getObjeto().getId().equals(e.getObjeto().getId())) {
                this.reservas.remove(r);
                return;
            }
        }
    }

    public void removeEmprestimo(Emprestimo e) {
        this.emprestimos.remove(e);
        this.emprestimosDevolvidos.add(e);
    }

    public int getMaxReservas() {
        return maxReservas;
    }

    public void setMaxReservas(int maxReservas) {
        this.maxReservas = maxReservas;
    }

    public void adicionaReserva(Reserva r) throws BusinessException {
        for (Reserva reserva : this.reservas) {
            if (reserva.getObjeto().getId().equals(r.getObjeto().getId())) {
                throw new BusinessException(getUsuarioEObjetoReturnMessage("Usuario ja possui reserva para este livro", r.getUsuario(), r.getObjeto()));
            }
        }

        this.reservas.add(r);
    }

    public void removeReserva(Reserva r) {
        this.reservas.remove(r);
    }

    public ArrayList<Emprestimo> getEmprestimos() {
        return emprestimos;
    }

    public ArrayList<Emprestimo> getEmprestimosDevolvidos() {
        return emprestimosDevolvidos;
    }

    public ArrayList<Reserva> getReservas() {
        return reservas;
    }

    @Override
    public String getObjectInfo() {
        StringBuilder resp = new StringBuilder("Usuario: " + this.getNome() + "\n\n");
        resp.append("Emprestimos:\n");

        if (this.emprestimos.size() == 0 && this.emprestimosDevolvidos.size() == 0) {
            resp.append("O usuario nao possui emprestimos\n");
        }

        for (Emprestimo e : this.emprestimosDevolvidos) {
            resp.append("Finalizado: ").append(e.getObjeto().getNome()).append(" | Emprestado em ").append(getFormattedDate(e.getDataEmprestimo()))
                    .append(" | Devolvido em ").append(getFormattedDate(e.getDataEntrega())).append("\n");
        }

        for (Emprestimo e : this.emprestimos) {
            resp.append("Em curso: ").append(e.getObjeto().getNome()).append(" | Emprestado em ").append(getFormattedDate(e.getDataEmprestimo()))
                    .append(" | Previsao de devolucao em ").append(getFormattedDate(e.getDataEntregaPrevista())).append("\n");
        }

        resp.append("\nReservas:\n");
        if (this.reservas.size() == 0) {
            resp.append("O usuario nao possui reservas\n");
        }

        for (Reserva r : this.reservas) {
            resp.append(r.getObjeto().getNome()).append(" | Solicitado em ").append(getFormattedDate(r.getDataEmprestimo())).append("\n");
        }

        return resp.toString();
    }
}

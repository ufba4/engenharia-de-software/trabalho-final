package main.usuario;

public class AlunoGraduacao extends Usuario {
    public AlunoGraduacao(String nome, String id) {
        super(nome, id, new ValidaEmprestimoAluno());
        this.setDiasEmprestimo(3);
        this.setMaxEmprestimos(3);
    }
}

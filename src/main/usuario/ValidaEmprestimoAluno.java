package main.usuario;

import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.Emprestimo;
import main.sistema.IDataSalvo;
import main.sistema.Reserva;

import java.util.ArrayList;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class ValidaEmprestimoAluno implements IValidaEmprestimo {
    @Override
    public Tuple2<Boolean, String> validaEmprestimo(IDataSalvo s, Objeto o, Usuario u) {
        boolean usuarioTemReserva = false;
        ArrayList<Reserva> reservas = s.pegaReservaIdObjeto(o.getId());
        ArrayList<Emprestimo> emprestimos = s.pegaEmprestimosIdObjeto(o.getId());

        if (u.getMaxEmprestimos() < u.getEmprestimos().size()) {
            return new Tuple2<>(false, getUsuarioEObjetoReturnMessage("Limite de emprestimos em andamento atingido", u, o));
        }

        for (Emprestimo e : emprestimos) {
            if (e != null && e.getUsuario().getId().equals(u.getId())) {
                return new Tuple2<>(false, getUsuarioEObjetoReturnMessage("Usuario ja possui um emprestimo em andamento para este livro", u, o));
            }
        }

        for (Reserva r : reservas) {
            if (r.getUsuario().getId().equals(u.getId())) {
                usuarioTemReserva = true;
                break;
            }
        }

        int numeroDisponiveis = o.getNumeroDisponiveis();
        if (u.isDevedor()) {
            return new Tuple2<>(false, getUsuarioEObjetoReturnMessage("Usuario possui um ou mais emprestimos atrasados", u, o));
        }

        if (!usuarioTemReserva && reservas.size() >= numeroDisponiveis) {
            return new Tuple2<>(false, getUsuarioEObjetoReturnMessage("Todos os exemplares foram reservados", u, o));
        }

        return new Tuple2<>(true, null);
    }
}

package main.usuario;

import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;

import static main.comunicacao.MessageUtils.getUsuarioEObjetoReturnMessage;

public class ValidaEmprestimoProfessor implements IValidaEmprestimo {
    @Override
    public Tuple2<Boolean, String> validaEmprestimo(IDataSalvo s, Objeto o, Usuario u) {
        return new Tuple2<>(!u.isDevedor(), getUsuarioEObjetoReturnMessage("Usuario possui um ou mais emprestimos atrasados", u, o));
    }
}

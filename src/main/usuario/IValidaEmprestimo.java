package main.usuario;

import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;

public interface IValidaEmprestimo {
    Tuple2<Boolean, String> validaEmprestimo(IDataSalvo s, Objeto o, Usuario u);
}

package main.usuario;

public class Professor extends Usuario implements IAdministrador {
    private int numeroNotificacoes;
    public Professor(String nome, String id) {
        super(nome, id, new ValidaEmprestimoProfessor());
        this.setDiasEmprestimo(7);
        this.numeroNotificacoes = 0;
    }

    @Override
    public void notifica() {
        this.numeroNotificacoes++;
    }

    public int getNumeroNotificacoes() {
        return numeroNotificacoes;
    }
}

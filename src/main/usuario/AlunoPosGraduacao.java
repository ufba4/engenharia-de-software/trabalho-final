package main.usuario;

public class AlunoPosGraduacao extends Usuario {
    public AlunoPosGraduacao(String nome, String id) {
        super(nome, id, new ValidaEmprestimoAluno());
        this.setDiasEmprestimo(3);
        this.setMaxEmprestimos(4);
    }
}

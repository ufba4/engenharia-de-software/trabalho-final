package main.internals;

public class Tuple2<X, Y> {
    private final X primeiroElemento;
    private final Y segundoElemento;

    public Tuple2(X firstElement, Y secondElement) {
        this.primeiroElemento = firstElement;
        this.segundoElemento = secondElement;
    }

    public X pegaPrimeiroElemento() {
        return primeiroElemento;
    }

    public Y pegaSegundoElemento() {
        return segundoElemento;
    }
}
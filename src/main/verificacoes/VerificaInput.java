package main.verificacoes;

import main.exceptions.InvalidCommandException;
import main.exceptions.ObjectNotFoundException;
import main.internals.Tuple2;
import main.objetos.Objeto;
import main.sistema.IDataSalvo;
import main.usuario.IAdministrador;
import main.usuario.Usuario;

public class VerificaInput {
    public static Tuple2<Usuario, Objeto> validaUsuarioEObjeto(String commandTotal, IDataSalvo sistema) throws InvalidCommandException, ObjectNotFoundException {
        String[] data = getCommandParameters(commandTotal);
        String usuarioID = data[1];
        String objetoID = data[2];

        Usuario u = sistema.pegarUsuarioPorId(usuarioID);
        Objeto o = sistema.pegarObjetoPorId(objetoID);

        if (u == null || o == null) {
            throw new ObjectNotFoundException("Usuario ou Livro nao encontrado");
        }

        return new Tuple2<>(u, o);
    }

    public static Tuple2<IAdministrador, Objeto> validaAdministradorEObjeto(String commandTotal, IDataSalvo sistema) throws InvalidCommandException, ObjectNotFoundException {
        String[] data = getCommandParameters(commandTotal);
        String usuarioID = data[1];
        String objetoID = data[2];

        IAdministrador a = sistema.pegarAdministradorPorId(usuarioID);
        Objeto o = sistema.pegarObjetoPorId(objetoID);

        if (a == null || o == null) {
            throw new ObjectNotFoundException("Admin ou Livro nao encontrado");
        }

        return new Tuple2<>(a, o);
    }

    public static Objeto validaObjeto(String commandTotal, IDataSalvo sistema) throws InvalidCommandException, ObjectNotFoundException {
        String id = getSingleParam(commandTotal);

        Objeto o = sistema.pegarObjetoPorId(id);
        if (o == null) {
            throw new ObjectNotFoundException("Livro nao encontrado");
        }

        return o;
    }

    public static Usuario validaUsuario(String commandTotal, IDataSalvo sistema) throws InvalidCommandException, ObjectNotFoundException {
        String id = getSingleParam(commandTotal);

        Usuario u = sistema.pegarUsuarioPorId(id);
        if (u == null) {
            throw new ObjectNotFoundException("Usuario nao encontrado");
        }

        return u;
    }

    public static IAdministrador validaAdministrador(String commandTotal, IDataSalvo sistema) throws InvalidCommandException, ObjectNotFoundException {
        String id = getSingleParam(commandTotal);

        IAdministrador a = sistema.pegarAdministradorPorId(id);
        if (a == null) {
            throw new ObjectNotFoundException("Usuario nao encontrado");
        }

        return a;
    }

    private static String[] getCommandParameters(String command) throws InvalidCommandException {
        String[] data = command.split("\\s+");
        if (data.length == 3) {
            return data;
        }
        throw new InvalidCommandException("Parametros de comando invalidos.");
    }

    private static String getSingleParam(String command) throws InvalidCommandException {
        String[] data = command.split("\\s+");
        if (data.length == 2) {
            return data[1];
        }

        throw new InvalidCommandException("Parametros de comando invalidos.");
    }
}